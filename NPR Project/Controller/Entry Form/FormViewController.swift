//
//  FormViewController.swift
//  NPR Project
//
//  Created by Jake Sims on 4/23/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//
import UIKit


class FormViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedOutside()
    }
    
    @IBAction func cancelButtonSelected(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButtonSelected(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
