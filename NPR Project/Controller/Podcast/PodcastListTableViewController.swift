//
//  PodcastListTableViewController.swift
//  NPR Project
//
//  Created by Jake Sims on 4/26/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import UIKit

class PodcastListTableViewController: UITableViewController {
    
    var podcasts = [Podcast]()

    override func viewDidLoad() {
        super.viewDidLoad()
       DataRetriever.fetchPodcastData(for: self)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return podcasts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let podcast = podcasts[indexPath.row]
        let cell = Bundle.main.loadNibNamed("PodcastTableViewCell", owner: self, options: nil)?.first as! PodcastTableViewCell
        
        cell.titleLabel.text = podcast.title
        cell.authorLabel.text = podcast.author
        cell.pubDateLabel.text = podcast.pubDate
        cell.durationLabel.text = podcast.duration
        cell.descriptionLabel.text = podcast.description
        cell.podcaseImageView.downloadImage(from: podcast.imageURLString)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let podcast = podcasts[indexPath.row]
        performSegue(withIdentifier: Constants.audioPlayerSegue, sender: podcast)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.audioPlayerSegue {
            let audioPlayer = segue.destination as! PodcastPlayerViewController
            audioPlayer.podcast = sender as? Podcast
        }
    }
}
