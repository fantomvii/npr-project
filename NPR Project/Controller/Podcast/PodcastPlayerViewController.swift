//
//  PodcastPlayerViewController.swift
//  NPR Project
//
//  Created by Jake Sims on 5/1/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import UIKit
import AVFoundation

class PodcastPlayerViewController: UIViewController {

    var player: AVPlayer?
    var podcast: Podcast?
    var isPodcastDownloaded = false
    
    // MARK: - Outlets
    @IBOutlet var podcastImage: UIImageView!
    @IBOutlet var trackSlider: UISlider!
    
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var currentTimeLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var restartButton: UIButton!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var downloadButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let podcast = podcast {
            podcastImage.downloadImage(from: podcast.imageURLString)
            titleLabel.text = podcast.title
            authorLabel.text = podcast.author
            durationLabel.text = podcast.duration
            currentTimeLabel.text = Constants.initialDuration
        }
        setUpPlayer()
    }
    
    func setUpPlayer() {
        guard let podcastURLString =  podcast?.podcastUrlString, let podcastURL = URL(string: podcastURLString) else {return}
        player = AVPlayer(url: podcastURL)
        
        let interval = CMTime(value: 1, timescale: 2)
        player?.addPeriodicTimeObserver(forInterval: interval, queue: .main, using: { progressTime in
            let seconds = CMTimeGetSeconds(progressTime)
            let secondsString = String(format: Constants.timeFormat, Int(seconds.remainder(dividingBy: 60)))
            let minuteString = String(format: Constants.timeFormat, Int(seconds)/60 % 60)
            let hourString = String(format: Constants.timeFormat, Int(seconds)/3660)
            self.currentTimeLabel.text = "\(hourString):\(minuteString):\(secondsString)"
            
            if let duration = self.player?.currentItem?.duration {
                let durationSeconds = CMTimeGetSeconds(duration)
                self.trackSlider.value = Float(seconds/durationSeconds)
            }
            
        })
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func dragSlider(_ sender: Any) {
        if let duration = player?.currentItem?.duration {
            let totalSeconds = CMTimeGetSeconds(duration)
            let value = Float64(trackSlider.value) * totalSeconds
            let seekTime = CMTime(value: Int64(value), timescale: 1)
            player?.seek(to: seekTime)
        }
    }
    
    
    func downloadAudio(for podcast: Podcast) {
        if let podcastURL = URL(string: podcast.podcastUrlString) {
            let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let destinationURL = documentDirectoryURL.appendingPathComponent(podcastURL.lastPathComponent)
            
            if FileManager.default.fileExists(atPath: destinationURL.path) {
                return
            } else {
                URLSession.shared.downloadTask(with: podcastURL) { location, response, error in
                    guard let location = location, error != nil else { return }
                    do{
                        try FileManager.default.moveItem(at: location, to: destinationURL)
                    } catch {
                        print(error.localizedDescription)
                    }
                    
                }.resume()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        if player?.rate == 0  {
            player?.play()
            playButton.setImage(UIImage(named: Constants.pauseIcon), for: .normal)
        } else {
            player?.pause()
            playButton.setImage(UIImage(named: Constants.playIcon), for: .normal)
        }
    }
    
    @IBAction func downloadTrashPressed(_ sender: Any) {
        if !isPodcastDownloaded  {
            downloadButton.setImage(UIImage(named: Constants.trashIcon), for: .normal)
            isPodcastDownloaded = true
        } else {
            downloadButton.setImage(UIImage(named: Constants.downloadIcon), for: .normal)
            isPodcastDownloaded = false
        }
    }
    
    @IBAction func restartButtonPressed(_ sender: Any) {
            player?.seek(to: .zero)
            player?.play()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
