//
//  WebViewViewController.swift
//  NPR Project
//
//  Created by Jake Sims on 4/24/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, WKNavigationDelegate {

    var webview: WKWebView!
    var article: Article?
    
    override func loadView() {
        webview = WKWebView()
        webview.navigationDelegate = self
        view = webview
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let urlString = article?.url, let url = URL(string: urlString) {
            webview.load(URLRequest(url: url))
            webview.allowsBackForwardNavigationGestures = true
        }
    }
}
