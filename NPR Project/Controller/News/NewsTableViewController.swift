//
//  NewsTableViewController.swift
//  NPR Project
//
//  Created by Jake Sims on 4/24/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import UIKit

class NewsTableViewController: UITableViewController {
    
    var articles = [Article]()
    lazy var refresh: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(getArticles), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Da News"
        getArticles()
        tableView.refreshControl = refresh
    }
    
    @objc
    func getArticles() {
        DataRetriever.retrieveTopHeadlinesData() { (newsArticles: [Article]) in
            for article in newsArticles {
                self.articles.append(article)
            }
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
                self?.refresh.endRefreshing()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.navigateToWebView {
            let webview = segue.destination as! WebViewViewController
            webview.article = sender as? Article
        }
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let article = articles[indexPath.row]
        let cell = Bundle.main.loadNibNamed("NewsTableViewCell", owner: self, options: nil)?.first as! NewsTableViewCell
    
        cell.titleLabel.text = article.title
        cell.authorLabel.text = article.author
        cell.sourceLabel.text = article.sourceName
        
        cell.articleImageView.downloadImage(from: article.urlToImage)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = articles[indexPath.row]
        performSegue(withIdentifier: Constants.navigateToWebView, sender: article)
    }
}
