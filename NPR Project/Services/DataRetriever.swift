//
//  DataRetriever.swift
//  NPR Project
//
//  Created by Jake Sims on 4/23/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import Foundation

class DataRetriever: NSObject {
    
    static func retrieveTopHeadlinesData(completion: @escaping ([Article]) -> ()) {
        
        let headlinesURL = URL(string: Constants.headlinesPath)!
        var urlRequest = URLRequest(url: headlinesURL)
        urlRequest.setValue(Constants.bearer, forHTTPHeaderField: Constants.authorization)
        urlRequest.httpMethod = Constants.get
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            var topStories = [Article]()
            guard let data = data else { return }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    if let articles = json[Constants.articles] as? [[String: Any]] {
                        for jsonArticle in articles {
                            let article = Article(json: jsonArticle)
                            topStories.append(article)
                        }
                    }
                }
            } catch {
                print(error.localizedDescription)
            }
            
            completion(topStories)
        }.resume()
    }
    
    static func fetchPodcastData(for podcastViewController: PodcastListTableViewController) {
        let feedParser = XMLFeedParser()
        feedParser.parseFeed(url: Constants.anxietyAndUsURL) { (podcasts) in
            podcastViewController.podcasts = podcasts
            OperationQueue.main.addOperation {
                podcastViewController.tableView.reloadData()
            }
        }
    }
    
}
