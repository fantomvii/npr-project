//
//  XMLFeedParser.swift
//  NPR Project
//
//  Created by Jake Sims on 4/25/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import Foundation

class XMLFeedParser: NSObject {
    private var podcasts = [Podcast]()
    private var currentAuthor = ""
    private var currentElement = ""
    private var currentTitle = ""
    private var currentPubDate = ""
    private var currentLink = ""
    private var currentDescription = ""
    private var currentDuration = ""
    private var currentURLString = ""
    private var currentImageURLString = ""
    private var currentPodcastURLString = ""
    private var parserCompletionHandler: (([Podcast]) -> Void)?
    
    func parseFeed(url: String, completionHandler: (([Podcast]) -> Void)?) {
        parserCompletionHandler = completionHandler
        
        let urlRequest = URLRequest(url: URL(string: url)!)
        URLSession.shared.dataTask(with: urlRequest) { (data, urlResponse, error) in
            guard let data = data else {
                if let error = error {
                    print(error.localizedDescription)
                }
                return
            }
            
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }.resume()
    }
    
    private func formatDate(_ pubDateString: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = Constants.pubDateFormat
        
        let dateFormatterSet = DateFormatter()
        dateFormatterSet.dateFormat = Constants.dateFormat
        guard let pubDate = dateFormatterGet.date(from: pubDateString) else {return ""}
        return dateFormatterSet.string(from: pubDate)
    }
}

// MARK: - Delegate methods

extension XMLFeedParser: XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentElement = elementName
        
        if currentElement == Constants.item {
            currentTitle = ""
            currentPubDate = ""
            currentLink = ""
            currentDescription = ""
            currentDuration = ""
            currentAuthor = ""
        } else if currentElement == Constants.image {
            currentImageURLString = ""
        } else if currentElement == Constants.enclosure {
            if let urlString = attributeDict[Constants.url] {
                currentPodcastURLString = urlString
            }
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == Constants.item {
            let podcast = Podcast(title: currentTitle, author: currentAuthor, pubDate: currentPubDate, podcastUrlString: currentPodcastURLString, imageURLString: currentImageURLString, link: currentLink, description: currentDescription, duration: currentDuration)
            podcasts.append(podcast)
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        switch currentElement {
        case Constants.title:
            currentTitle += data
        case Constants.url:
            currentImageURLString += data
        case Constants.itunesAuthor:
            currentAuthor += data
        case Constants.pubDate:
            currentPubDate += formatDate(data)
        case Constants.description:
            currentDescription += data
        case Constants.link:
            currentLink += data
        case Constants.itunesDuration:
            currentDuration += data
        default:
            break
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        parserCompletionHandler?(podcasts)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError.localizedDescription)
    }
}
