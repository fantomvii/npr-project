//
//  UIViewController+HideKeyboard.swift
//  NPR Project
//
//  Created by Jake Sims on 5/2/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedOutside() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
