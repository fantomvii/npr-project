//
//  UIImageView+ImageFetcher.swift
//  NPR Project
//
//  Created by Jake Sims on 4/24/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func downloadImage(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                error == nil,
                let image = UIImage(data: data) else {return}
            OperationQueue.main.addOperation {
                self.image = image
            }
        }.resume()
    }
    
    func downloadImage(from urlString: String, mode: UIView.ContentMode = .scaleAspectFit) {
        if let url = URL(string: urlString) {
            downloadImage(from: url, contentMode: mode)
        }
    }
}
