//
//  Constants.swift
//  NPR Project
//
//  Created by Jake Sims on 4/23/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

struct Constants {
    
    // Dictionary Keys
    static let articles = "articles"
    static let title = "title"
    static let author = "author"
    static let description = "description"
    static let url = "url"
    static let urlToImage = "urlToImage"
    static let content = "content"
    
    // Images
    static let downloadIcon = "download.png"
    static let playIcon = "play.png"
    static let pauseIcon = "pause.png"
    static let restartIcon = "restart.png"
    static let trashIcon = "trash.png"

    // News API
    static let apiKey = "e0cd87a278c34adaa84c4ad2e5f32064"
    static let authorization = "Authorization"
    static let bearer = "Bearer \(apiKey)"
    static let get = "GET"
    static let headlinesPath = "https://newsapi.org/v2/top-headlines?country=us"
    
    // Podcast XML
    static let anxietyAndUsURL = "https://feeds.soundcloud.com/users/soundcloud:users:30772561/sounds.rss"
    static let item = "item"
    static let duration = "duration"
    static let enclosure = "enclosure"
    static let image = "image"
    static let initialDuration = "00:00:00"
    static let pubDate = "pubDate"
    static let link = "link"
    static let itunesDuration = "itunes:duration"
    static let itunesAuthor = "itunes:author"
    static let pubDateFormat = "E, dd MMM yyyy HH:mm:ssZ"
    static let dateFormat = "MMM dd"
    
    // Strings
    static let navigateToWebView = "NavigateToWebView"
    static let timeFormat = "%02i"
    
    // Segue
    static let audioPlayerSegue = "audioPlayerSegue"
}
