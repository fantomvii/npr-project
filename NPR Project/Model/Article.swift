//
//  News.swift
//  NPR Project
//
//  Created by Jake Sims on 4/23/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

struct Article {
    let id: String?
    let title: String
    let sourceName: String
    let author: String?
    let description: String
    let url: String
    let urlToImage: String
    let content: String?
    
    init(json: [String: Any]) {
        title = json[Constants.title] as? String ?? ""
        author = json[Constants.author] as? String ?? ""
        description = json[Constants.description] as? String ?? ""
        content = json[Constants.content] as? String ?? ""
        url = json[Constants.url] as? String ?? ""
        urlToImage = json[Constants.urlToImage] as? String ?? ""
        let jsonSource = json["source"] as? [String: Any] ?? [:]
        let source = Source(json: jsonSource)
        sourceName = source.name
        id = source.id
    }
}

struct Source {
    let name: String
    let id: String?
    
    init(json: [String: Any]) {
        name = json["name"] as? String ?? ""
        id = json["id"] as? String ?? ""
    }
}
