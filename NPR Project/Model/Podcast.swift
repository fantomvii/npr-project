//
//  RSSItem.swift
//  NPR Project
//
//  Created by Jake Sims on 4/25/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//
import UIKit
struct Podcast {
    var title: String
    var author: String
    var pubDate: String
    var podcastUrlString: String
    var imageURLString: String
    var link: String
    var description: String
    var duration: String
}
