//
//  Form.swift
//  NPR Project
//
//  Created by Jake Sims on 4/23/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

class Form {
    var name: String?
    var email: String?
    var genre: String?
    var nprStation: String?
    var musicians = [String]()
    var songwriter: [String]
    var videographer: String?
    var youtubeURLString: String
    
    init(name: String, email: String, genre: String, nprStation: String, musicians: [String], songwriter: [String], videographer: String, youtubeURLString: String) {
        self.name = name
        self.email = email
        self.genre = genre
        self.nprStation = nprStation
        self.musicians = musicians
        self.songwriter = songwriter
        self.videographer = videographer
        self.youtubeURLString = youtubeURLString
    }
}
