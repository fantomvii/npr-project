//
//  NewsTableViewCell.swift
//  NPR Project
//
//  Created by Jake Sims on 4/24/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet var articleImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var sourceLabel: UILabel!
    

}
