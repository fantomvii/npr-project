//
//  PodcastTableViewCell.swift
//  NPR Project
//
//  Created by Jake Sims on 4/26/19.
//  Copyright © 2019 Jake Sims. All rights reserved.
//

import UIKit

class PodcastTableViewCell: UITableViewCell {

    @IBOutlet var podcaseImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var pubDateLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    
}
